import { AppLocalidadesPage } from './app.po';

describe('app-localidades App', () => {
  let page: AppLocalidadesPage;

  beforeEach(() => {
    page = new AppLocalidadesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
