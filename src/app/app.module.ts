import { LocalidadService } from './services/localidad.service';
import { ProvinciaEditComponent } from './components/provincia-edit/provincia-edit.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ProvinciaListadoComponent } from './components/provincia-listado/provincia-listado.component';
import { ProvinciaFormComponent } from './components/provincia-form/provincia-form.component';
import { NavbarComponent } from './navbar/navbar.component';
import { APP_ROUTING } from './app.routes';
import { ProvinciaService } from './services/provincia.service';
import { HttpModule } from '@angular/http';
import { LocalidadEditComponent } from './components/localidad-edit/localidad-edit.component';
import { LocalidadFormComponent } from './components/localidad-form/localidad-form.component';
import { LocalidadListadoComponent } from './components/localidad-listado/localidad-listado.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProvinciaListadoComponent,
    ProvinciaFormComponent,
    NavbarComponent,
    ProvinciaEditComponent,
    LocalidadEditComponent,
    LocalidadFormComponent,
    LocalidadListadoComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpModule,
    FormsModule
  ],
  providers: [ProvinciaService, LocalidadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
