import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ProvinciaListadoComponent } from "./components/provincia-listado/provincia-listado.component";
import { ProvinciaFormComponent } from "./components/provincia-form/provincia-form.component";
import { ProvinciaEditComponent } from "./components/provincia-edit/provincia-edit.component";
import { LocalidadListadoComponent } from "./components/localidad-listado/localidad-listado.component";
import { LocalidadFormComponent } from "./components/localidad-form/localidad-form.component";
import { LocalidadEditComponent } from "./components/localidad-edit/localidad-edit.component";

const ROUTES:Routes = [
    {
        path: "home", component: HomeComponent
    },
    {
        path: "provincias", component: ProvinciaListadoComponent
    },
    {
        path: "provincias/nuevo", component: ProvinciaFormComponent
    },
    {
        path: "provincias/editar", component: ProvinciaEditComponent
    },
    {
        path: "localidades", component: LocalidadListadoComponent
    },
    {
        path: "localidades/nuevo", component: LocalidadFormComponent
    },
    {
        path: "localidades/editar", component: LocalidadEditComponent
    },
    {
        path:"**", pathMatch: "full", redirectTo:"home"
    }
]

export const APP_ROUTING = RouterModule.forRoot(ROUTES);