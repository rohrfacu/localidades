export class Localidad {
    id: number;
    nombre: string;
    cp: number;
    idprovincia: number;

    constructor(id: number, nombre: string, cp: number, idprovincia?: number)
    {
        this.id = id;
        this.nombre = nombre;
        this.cp = cp;
        this.idprovincia = idprovincia;
    }
}
