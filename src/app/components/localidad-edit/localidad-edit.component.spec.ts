import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalidadEditComponent } from './localidad-edit.component';

describe('LocalidadEditComponent', () => {
  let component: LocalidadEditComponent;
  let fixture: ComponentFixture<LocalidadEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalidadEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalidadEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
