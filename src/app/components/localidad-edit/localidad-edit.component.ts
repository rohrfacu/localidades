import { Component, OnInit } from '@angular/core';
import { LocalidadService } from '../../services/localidad.service';
import { Localidad } from '../../classes/localidad';

@Component({
  selector: 'app-localidad-edit',
  templateUrl: './localidad-edit.component.html',
  styleUrls: ['./localidad-edit.component.css']
})
export class LocalidadEditComponent implements OnInit {

  localidad: Localidad;
  
    constructor(private localidadService: LocalidadService) {
      this.localidad = localidadService.localidad;
     }
  
    ngOnInit() {
    }
  
    editarLocalidad(){
      this.localidadService.editarLocalidad(this.localidad);
    }

}
