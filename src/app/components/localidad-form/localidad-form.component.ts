import { Component, OnInit } from '@angular/core';
import { LocalidadService } from '../../services/localidad.service';
import { Localidad } from '../../classes/localidad';

@Component({
  selector: 'app-localidad-form',
  templateUrl: './localidad-form.component.html',
  styleUrls: ['./localidad-form.component.css']
})
export class LocalidadFormComponent implements OnInit {

  localidad:Localidad = new Localidad(1,"", 1);
  
    constructor(private localidadService: LocalidadService) { }
  
    ngOnInit() {
    }
  
    crearLocalidad(){
      this.localidadService.nuevaLocalidad(this.localidad)
      .then(() =>{
        alert('Se creo la localidad');
      })
    }

}
