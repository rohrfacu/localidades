import { ProvinciaService } from './../../services/provincia.service';
import { Provincia } from './../../classes/provincia';
import { Component, OnInit } from '@angular/core';
import { Localidad } from '../../classes/localidad';
import { LocalidadService } from '../../services/localidad.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-localidad-listado',
  templateUrl: './localidad-listado.component.html',
  styleUrls: ['./localidad-listado.component.css']
})
export class LocalidadListadoComponent implements OnInit {

    nombreProvincia:string = 'No definido!';

    constructor(private localidadService: LocalidadService, private router:Router, private provinciaservice:ProvinciaService){
    }
    
    ngOnInit() {
      
    }
  
    editar(localidadAeditar: Localidad){
      this.localidadService.localidad = localidadAeditar;
      this.router.navigate(['localidades/editar'])
    }

}
