import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvinciaEditComponent } from './provincia-edit.component';

describe('ProvinciaEditComponent', () => {
  let component: ProvinciaEditComponent;
  let fixture: ComponentFixture<ProvinciaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvinciaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvinciaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
