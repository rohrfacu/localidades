import { Provincia } from './../../classes/provincia';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ProvinciaService } from '../../services/provincia.service';

@Component({
  selector: 'app-provincia-edit',
  templateUrl: './provincia-edit.component.html',
  styleUrls: ['./provincia-edit.component.css']
})
export class ProvinciaEditComponent implements OnInit {

  provincia: Provincia;

  constructor(private provinciaService: ProvinciaService) {
    this.provincia = provinciaService.provincia;
   }

  ngOnInit() {
  }

  editarProvincia(){
    this.provinciaService.editarProvincia(this.provincia);
  }
}
