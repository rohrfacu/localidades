import { Component, OnInit } from '@angular/core';
import { ProvinciaService } from '../../services/provincia.service';
import { Provincia } from '../../classes/provincia';

@Component({
  selector: 'app-provincia-form',
  templateUrl: './provincia-form.component.html',
  styleUrls: ['./provincia-form.component.css']
})
export class ProvinciaFormComponent implements OnInit {

  provincia:Provincia = new Provincia(1,"");

  constructor(private provinciaService: ProvinciaService) { }

  ngOnInit() {
  }

  crearProvincia(){
    this.provinciaService.nuevaProvincia(this.provincia)
    .then(() =>{
      alert('Se creo la provincia');
    })
  }

}
