import { Provincia } from './../../classes/provincia';
import { Component, OnInit } from '@angular/core';
import { ProvinciaService} from '../../services/provincia.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provincia-listado',
  templateUrl: './provincia-listado.component.html',
  styleUrls: ['./provincia-listado.component.css']
})
export class ProvinciaListadoComponent implements OnInit {

  constructor(private provinciaService: ProvinciaService, private router: Router) { }

  ngOnInit() {
  }

  editar(provinciaAeditar: Provincia){
    this.provinciaService.provincia = provinciaAeditar;
    this.router.navigate(['provincias/editar'],)
  }
}
