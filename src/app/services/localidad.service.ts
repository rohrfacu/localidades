import { Localidad } from './../classes/localidad';
import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


@Injectable()
export class LocalidadService {
  listadoLocalidad: Localidad[] = [];
  localidad: Localidad;

  constructor(private http:Http, private router: Router) { 
    this.obtenerListado();
  }

  obtener(id:number){
    this.http.get("http://localhost:3000/localidad/"+id).subscribe((res:Response) => {
      let localidad = res.json();
      localidad.array.forEach(element => {
        this.localidad = new Localidad(element.id, element.nombre, element.cp);
      });
    })
  }

  obtenerListado(){
    this.listadoLocalidad = []
    this.http.get("http://localhost:3000/localidades").subscribe((res:Response) => {
      let localidads = res.json();
      localidads.forEach(localidad => {
        this.listadoLocalidad.push(new Localidad(localidad.id, localidad.nombre, localidad.cp, localidad.idprovincia));
      });
      this.listadoLocalidad.sort((prov1, prov2) => prov1.id - prov2.id);
    })
  }

  devolverListado():Localidad[]{
    return this.listadoLocalidad;
  }

  nuevaLocalidad(localidad){
    return new Promise((resolve, reject) =>{
      this.http.post("http://localhost:3000/localidades",localidad).subscribe((res:Response) =>{
        this.obtenerListado();
        this.router.navigate(['localidades'])
        resolve();
      })
    })
  }
  editarLocalidad(localidad){
    return new Promise((resolve, reject) =>{
      this.http.patch("http://localhost:3000/localidades"+"/"+localidad.id,localidad).subscribe((res:Response) =>{
        this.obtenerListado();
        this.router.navigate(['localidades'])
        resolve();
      })
    })
  }
  eliminarLocalidad(id:number){
    console.log(id);
    this.http.delete("http://localhost:3000/localidades"+"/"+id).subscribe((res : Response) =>{
      let data = res.json();
      this.obtenerListado();
      this.router.navigate(['localidades'])
  })
  }
}
