import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Provincia } from '../classes/provincia';
import { Router } from '@angular/router';

@Injectable()
export class ProvinciaService {

  listadoProvincia: Provincia[] = [];
  provincia: Provincia;

  constructor(private http:Http, private router:Router) { 
    this.obtenerListado();
  }

  obtener(id:number){
      this.http.get("http://localhost:3000/provincias/"+id).subscribe((res:Response) => {
        let provincia = res.json();
        provincia.array.forEach(element => {
          this.provincia = new Provincia(element.id, element.nombre);
        });
        return this.provincia.nombre;
      })
  }

  obtenerListado(){
    this.listadoProvincia = []
    this.http.get("http://localhost:3000/provincias").subscribe((res:Response) => {
      let provincias = res.json();
      provincias.forEach(provincia => {
        this.listadoProvincia.push(new Provincia(provincia.id, provincia.nombre));
      });
      this.listadoProvincia.sort((prov1, prov2) => prov1.id - prov2.id);
    })
  }

  devolverListado():Provincia[]{
    return this.listadoProvincia;
  }

  nuevaProvincia(provincia){
    return new Promise((resolve, reject) =>{
      this.http.post("http://localhost:3000/provincias",provincia).subscribe((res:Response) =>{
        this.obtenerListado();
        this.router.navigate(['provincias'])
        resolve();
      })
    })
  }
  editarProvincia(provincia){
    return new Promise((resolve, reject) =>{
      this.http.patch("http://localhost:3000/provincias"+"/"+provincia.id,provincia).subscribe((res:Response) =>{
        this.obtenerListado();
        this.router.navigate(['provincias'])
        resolve();
      })
    })
  }
  eliminarProvincia(id:number){
    this.http.delete("http://localhost:3000/provincias"+"/"+id).subscribe((res : Response) =>{
      let data = res.json();
      this.obtenerListado();
      this.router.navigate(['provincias'])
  })
  }
}
